import requests
import json
from tkinter import ttk
import tkinter as tk
from tkinter.scrolledtext import ScrolledText

token = "KiQSsELLtocyT75plaYc0_XbTucWfBDueYJoj6eH"
headers = {'Authorization':'Bearer '+token}
baseUrl = "https://vendapitest.vendhq.com/"
outlet = None

def PerformAPIGet(endpoint,queryParams,newAPI):
	apiURL = ""
	if newAPI:
		apiURL=baseUrl+"api/2.0/"
	else:
		apiURL=baseUrl+"api/"
	query = apiURL+endpoint+"?"
	for param in queryParams:
		query = query+param
		query = query+"&"
	get = requests.get(query,headers=headers)
	return ContentToJson(get.content)

def ContentToJson(content):
	return json.loads(content)

def GetAllProducts():
	products = PerformAPIGet("products",["page_size=2000"],True)
	return products

def GetSchullOutlet():
	outlets = PerformAPIGet("outlets",[],False)["outlets"]
	retOutlet = None
	for outleti in outlets:
		if outleti["name"] == "Schull":
			retOutlet = outleti
	global outlet
	outlet = retOutlet

GetSchullOutlet()

def PerformAPIPost(endpoint,requestBody,newAPI):
	apiURL = ""
	if newAPI:
		apiURL=baseUrl+"api/2.0/"
	else:
		apiURL=baseUrl+"api/"
	apiURL = apiURL+endpoint
	request = requests.post(apiURL,data=requestBody,headers=headers)
	return request.content	




def CreateSupplierOrder(outletID,supplier):

	data = {"name":"Test Consignment","outlet_id":outletID,"status":"OPEN","type":"SUPPLIER"}
	
	result = PerformAPIPost("consignment",json.dumps(data),False)
	print(result)

def GetOpenConsignments(outletID):
	result = PerformAPIGet("consignment",['page_size=50'],False)
	returnConsignments = []
	for i in result["consignments"]:
		if(i["outlet_id"]==outletID):
			returnConsignments.append(i)
	return returnConsignments

class MainWindow(tk.Frame):
	consignmentWindow = None
	consignmentWidgets = []
	consignments = []
	def __init__(self, *args, **kwargs):
		tk.Frame.__init__(self, *args, **kwargs)
		self.nb = ttk.Notebook(self)
		self.RenderConsignmentListWindow()
		self.RenderCreateWindow()

		self.nb.add(self.createTab,text="Create")
		self.nb.add(self.consignmentTab,text="Open Orders")
		self.nb.pack(expand=1,fill="both")
		self.RenderConsignments()

	def RenderConsignments(self):
		for widget in self.consignmentWidgets:
			widget.destroy()
		self.consignmentWidgets = [] 

		polledConsignments = GetOpenConsignments(outlet["id"])

		self.consignments = polledConsignments
		self.nameHeader = ttk.Label(self.consignmentTabGrid,text="Order Name",borderwidth=2,relief="solid",anchor="center")
		self.dateHeader = ttk.Label(self.consignmentTabGrid,text="Order date",borderwidth=2,relief="solid",anchor="center")
		self.supplierHeader = ttk.Label(self.consignmentTabGrid,text="Order supplier",borderwidth=2,relief="solid",anchor="center")
		self.editHeader = ttk.Label(self.consignmentTabGrid,text="Edit Order",borderwidth=2,relief="solid",anchor="center")
		self.nameHeader.grid(column=0,row=0,sticky="NSEW",ipadx=5,ipady=5)
		self.dateHeader.grid(column=1,row=0,sticky="NSEW",ipadx=5,ipady=5)
		self.supplierHeader.grid(column=2,row=0,sticky="NSEW",ipadx=5,ipady=5)
		self.editHeader.grid(column=3,row=0,sticky="NSEW",ipadx=5,ipady=5)
		for index in range(0,self.consignments.__len__()):
			print(index)
			widget = ttk.Button(self.consignmentTabGrid,text=self.consignments[index]["name"],command=lambda d=index:self.CreateConsignmentEditorWindow(d))
			widget.grid(column=0,row=index+1,ipadx=5,ipady=5)
			self.consignmentWidgets.append(widget)

	def RenderCreateWindow(self):
		self.createTab = ttk.Frame(self.nb)

	def RenderConsignmentListWindow(self):
		self.consignmentTab = ttk.Frame(self.nb)
		self.consignmentTabHeader = ttk.Frame(self.consignmentTab)
		self.consignmentTabGrid = ttk.Frame(self.consignmentTab)
		self.refreshButton = ttk.Button(self.consignmentTabHeader,text="Refresh Orders",command=self.RenderConsignments).pack()
		self.consignmentTabHeader.pack(side="top")
		self.consignmentTabGrid.pack(side="top")
	def HandleWindowClose(self,window):
		window = None

	def CreateConsignmentEditorWindow(self,consignmentIndex):
		print(consignmentIndex)
		print(self)
		try:
			if not(self.consignmentWindow.winfo_exists()):
				self.consignmentWindow = None
		except:
			pass
		if self.consignmentWindow == None:
			self.consignmentWindow = tk.Toplevel(self)
			self.consignmentWindow.wm_title("Editing order: %s"%self.consignments[consignmentIndex]["name"])
			l = tk.Label(self.consignmentWindow,text=str(self.consignments[consignmentIndex]),wraplength=500)
			l.pack(side="top",fill="both",expand=True,padx=100,pady=100)


if __name__ == "__main__":
	root = tk.Tk()
	main = MainWindow(root)
	main.pack(side="top",fill="both",expand=True)
	root.mainloop()